CREATE SCHEMA IF NOT EXISTS pgclients AUTHORIZATION postgres;

SET search_path TO extensions, pgclients;

CREATE TABLE IF NOT EXISTS pgclients.clients
(
    id uuid DEFAULT uuid_generate_v4(),
    firstname VARCHAR,
    lastname VARCHAR,
    company_name VARCHAR,
    address VARCHAR NOT NULL,
    phone_number VARCHAR,
    mail VARCHAR,
    PRIMARY KEY (id),
    CONSTRAINT need_client_full_name_or_company_name CHECK ( ( ( firstname IS NOT NULL ) AND ( lastname IS NOT NULL ) ) OR ( company_name IS NOT NULL ) )
);

COMMENT ON TABLE pgclients.clients IS 'Contains clients data.';
COMMENT ON COLUMN pgclients.clients.id IS 'The table''s arbitrary primary key.';
COMMENT ON COLUMN pgclients.clients.firstname IS 'The client first name (Optional if company_name is filled).';
COMMENT ON COLUMN pgclients.clients.lastname IS 'The client last name (Optional if company_name is filled).';
COMMENT ON COLUMN pgclients.clients.company_name IS 'The company name associated to the client (Optional if first and last name are filled).';
COMMENT ON COLUMN pgclients.clients.address IS 'The client address.';
COMMENT ON COLUMN pgclients.clients.phone_number IS 'The client phone number.';
COMMENT ON COLUMN pgclients.clients.mail IS 'The client mail.';
