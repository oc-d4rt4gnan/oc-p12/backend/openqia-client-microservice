package fr.d4rt4gnan.openqia.client.dao;


import fr.d4rt4gnan.openqia.client.model.ClientBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author D4RT4GNaN
 * @since 03/01/2022
 */
@Repository
public interface ClientRepository extends PagingAndSortingRepository<ClientBean, UUID> {}