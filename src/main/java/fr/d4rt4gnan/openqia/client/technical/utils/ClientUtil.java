package fr.d4rt4gnan.openqia.client.technical.utils;

/**
 * @author D4RT4GNaN
 * @since 09/01/2022
 */
public interface ClientUtil {
    
    /**
     * Compare two data to know if the field need to be updated.
     *
     * @param inputValue - Input Data.
     * @param comparatorValue - Data in database for comparison.
     * @return true, if the value is different and not null or empty.
     */
    boolean isUpdatable(String inputValue, String comparatorValue);
    
}
