package fr.d4rt4gnan.openqia.client.technical.utils.impl;


import fr.d4rt4gnan.openqia.client.technical.utils.ClientUtil;

import javax.inject.Named;


/**
 * @author D4RT4GNaN
 * @since 09/01/2022
 */
@Named(value = "clientUtil")
public class ClientUtilImpl implements ClientUtil {
    
    @Override
    public boolean isUpdatable (final String inputValue, final String comparatorValue) {
        return inputValue != null
            && !inputValue.isEmpty()
            && inputValue.compareTo(comparatorValue) != 0;
    }
    
}
