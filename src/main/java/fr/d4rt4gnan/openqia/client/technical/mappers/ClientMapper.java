package fr.d4rt4gnan.openqia.client.technical.mappers;


import fr.d4rt4gnan.openqia.client.model.Client;
import fr.d4rt4gnan.openqia.client.model.ClientBean;
import fr.d4rt4gnan.openqia.client.technical.utils.ClientUtil;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * @author D4RT4GNaN
 * @since 03/01/2022
 */
public interface ClientMapper {
    
    /**
     * Allows to convert the Client object from the database version to
     * the transfer version via the swagger.
     *
     * @param clientBean - The Client object in database format.
     * @return the Client object in transfer format via swagger.
     */
    Client toClient(ClientBean clientBean);
    
    
    
    /**
     * Convert the client object from the swagger version to the database
     * version.
     *
     * @param client - The Client object in transfer format via swagger.
     * @return the Client object in database format.
     */
    ClientBean toClientBean(Client client);
    
    
    
    /**
     * Allows you to convert a list of Client objects from the database version to
     * the transfer version via the swagger.
     *
     * @param clientBeans - The Client object list of the database version.
     * @return the Client object list in transfer format via swagger.
     */
    List<Client> toClients (Page<ClientBean> clientBeans);
    
    
    
    /**
     * Update only the different fields and not null.
     *
     * @param client
     *         - Input data.
     * @param clientBean
     *         - Data in database for comparison.
     *
     * @return a client object in database format with updated fields.
     */
    ClientBean updateClientBean (Client client, ClientBean clientBean);
    
    
    
    /**
     * Setter of the "clientUtil" attribute for the dependency
     * injection.
     *
     * @param value
     *         - The value assigned.
     */
    void setClientUtil (ClientUtil value);
    
}
