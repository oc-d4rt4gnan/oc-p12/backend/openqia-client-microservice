package fr.d4rt4gnan.openqia.client.technical.mappers.impl;


import fr.d4rt4gnan.openqia.client.model.Client;
import fr.d4rt4gnan.openqia.client.model.ClientBean;
import fr.d4rt4gnan.openqia.client.technical.mappers.ClientMapper;
import fr.d4rt4gnan.openqia.client.technical.utils.ClientUtil;
import org.springframework.data.domain.Page;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author D4RT4GNaN
 * @since 03/01/2022
 */
@Named(value = "clientMapper")
public class ClientMapperImpl implements ClientMapper {
    
    /**
     * Utility dependency
     */
    private ClientUtil clientUtil;
    
    /**
     * @inheritDoc
     */
    @Override
    @Inject
    public void setClientUtil (final ClientUtil value) {
        this.clientUtil = value;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public Client toClient (final ClientBean clientBean) {
        Client client = new Client();
        
        client.setId(clientBean.getId());
        client.setFirstName(clientBean.getFirstName());
        client.setLastName(clientBean.getLastName());
        client.setCompanyName(clientBean.getCompanyName());
        client.setAddress(clientBean.getAddress());
        client.setPhoneNumber(clientBean.getPhoneNumber());
        client.setEmail(clientBean.getEmail());
        
        return client;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ClientBean toClientBean (final Client client) {
        ClientBean clientBean = new ClientBean();
        
        clientBean.setId(client.getId());
        clientBean.setFirstName(client.getFirstName());
        clientBean.setLastName(client.getLastName());
        clientBean.setCompanyName(client.getCompanyName());
        clientBean.setAddress(client.getAddress());
        clientBean.setPhoneNumber(client.getPhoneNumber());
        clientBean.setEmail(client.getEmail());
        
        return clientBean;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public List<Client> toClients (final Page<ClientBean> clientBeans) {
        List<ClientBean> clientBeanList = clientBeans.toList();
        return clientBeanList.stream().map(this::toClient).collect(Collectors.toList());
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ClientBean updateClientBean (
            final Client client,
            ClientBean clientBean
    ) {
        clientBean = updateFirstName(client, clientBean);
        clientBean = updateLastName(client, clientBean);
        clientBean = updateCompanyName(client, clientBean);
        clientBean = updateAddress(client, clientBean);
        clientBean = updateEmail(client, clientBean);
        clientBean = updatePhoneNumber(client, clientBean);
        return clientBean;
    }

    /**
     * Update first name only if the data in input is different from its database counterpart.
     * 
     * @param client - Input data.
     * @param clientBean - Data in database for comparison.
     * @return a client object in database format with updated first name.
     */
    private ClientBean updateFirstName (Client client, ClientBean clientBean) {
        if (Boolean.TRUE.equals(
                clientUtil.isUpdatable(
                        client.getFirstName(),
                        clientBean.getFirstName()
                )
            )
        ) {
            clientBean.setFirstName(client.getFirstName());
        }
        return clientBean;
    }

    /**
     * Update last name only if the data in input is different from its database counterpart.
     * 
     * @param client - Input data.
     * @param clientBean - Data in database for comparison.
     * @return a client object in database format with updated last name.
     */
    private ClientBean updateLastName (Client client, ClientBean clientBean) {
        if (Boolean.TRUE.equals(
                clientUtil.isUpdatable(
                        client.getLastName(),
                        clientBean.getLastName()
                )
            )
        ) {
            clientBean.setLastName(client.getLastName());
        }
        return clientBean;
    }

    /**
     * Update company name only if the data in input is different from its database counterpart.
     * 
     * @param client - Input data.
     * @param clientBean - Data in database for comparison.
     * @return a client object in database format with updated company name.
     */
    private ClientBean updateCompanyName (Client client, ClientBean clientBean) {
        if (Boolean.TRUE.equals(
                clientUtil.isUpdatable(
                        client.getCompanyName(),
                        clientBean.getCompanyName()
                )
            )
        ) {
            clientBean.setCompanyName(client.getCompanyName());
        }
        return clientBean;
    }

    /**
     * Update address only if the data in input is different from its database counterpart.
     * 
     * @param client - Input data.
     * @param clientBean - Data in database for comparison.
     * @return a client object in database format with updated address.
     */
    private ClientBean updateAddress (Client client, ClientBean clientBean) {
        if (Boolean.TRUE.equals(
                clientUtil.isUpdatable(
                        client.getAddress(),
                        clientBean.getAddress()
                )
            )
        ) {
            clientBean.setAddress(client.getAddress());
        }
        return clientBean;
    }

    /**
     * Update email only if the data in input is different from its database counterpart.
     * 
     * @param client - Input data.
     * @param clientBean - Data in database for comparison.
     * @return a client object in database format with updated email.
     */
    private ClientBean updateEmail (Client client, ClientBean clientBean) {
        if (Boolean.TRUE.equals(
                clientUtil.isUpdatable(
                        client.getEmail(),
                        clientBean.getEmail()
                )
            )
        ) {
            clientBean.setEmail(client.getEmail());
        }
        return clientBean;
    }

    /**
     * Update phone number only if the data in input is different from its database counterpart.
     * 
     * @param client - Input data.
     * @param clientBean - Data in database for comparison.
     * @return a client object in database format with updated phone number.
     */
    private ClientBean updatePhoneNumber (Client client, ClientBean clientBean) {
        if (Boolean.TRUE.equals(
                clientUtil.isUpdatable(
                        client.getPhoneNumber(),
                        clientBean.getPhoneNumber()
                )
            )
        ) {
            clientBean.setPhoneNumber(client.getPhoneNumber());
        }
        return clientBean;
    }

}
