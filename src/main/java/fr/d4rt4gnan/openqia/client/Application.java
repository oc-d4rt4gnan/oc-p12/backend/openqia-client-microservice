package fr.d4rt4gnan.openqia.client;

import fr.d4rt4gnan.openqia.client.api.ClientsApiController;
import org.springdoc.core.SpringDocUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	
	/**
	 * Main class allowing to launch the Client Microservice
	 * with SpringBoot.
	 *
	 * @param args - Parameters launched with the Java command
	 */
	public static void main(final String[] args) {
		SpringDocUtils.getConfig().addRestControllers(ClientsApiController.class);
		SpringApplication.run(Application.class, args);
	}

}
