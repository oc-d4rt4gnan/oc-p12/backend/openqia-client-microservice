package fr.d4rt4gnan.openqia.client.business;

import fr.d4rt4gnan.openqia.client.api.ClientsApiDelegate;
import fr.d4rt4gnan.openqia.client.dao.ClientRepository;
import fr.d4rt4gnan.openqia.client.model.Client;
import fr.d4rt4gnan.openqia.client.model.ClientBean;
import fr.d4rt4gnan.openqia.client.technical.mappers.ClientMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author D4RT4GNaN
 * @since 03/01/2022
 */
@Service(value = "clientsApiDelegate")
public class ClientService implements ClientsApiDelegate {
    
    /**
     * ClientMapper dependency
     */
    private ClientMapper clientMapper;
    
    /**
     * ClientRepository dependency
     */
    private ClientRepository clientRepository;
    
    /**
     * Injection of ClientMapper dependencies into the service
     *
     * @param clientMapper - The ClientMapper to inject
     */
    @Inject
    public void setClientMapper (ClientMapper clientMapper) {
        this.clientMapper = clientMapper;
    }
    
    /**
     * Injection of ClientRepository dependencies into the service
     *
     * @param clientRepository - The ClientRepository to inject
     */
    @Inject
    public void setClientRepository (ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Client> addClient (
            final Client client
    ) {
        ClientBean clientBean = clientMapper.toClientBean(client);
        ClientBean clientBeanSaved = clientRepository.save(clientBean);
        return new ResponseEntity<>(clientMapper.toClient(clientBeanSaved), HttpStatus.CREATED);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Client> getClient (
            final UUID id
    ) {
        Optional<ClientBean> clientDB = clientRepository.findById(id);
        return clientDB.map(clientBean -> new ResponseEntity<>(clientMapper.toClient(clientBean), HttpStatus.OK))
                       .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<List<Client>> getClients(
            final Integer pageNumber,
            final Integer pageSize
    ) {
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize);
        Page<ClientBean> clientBeans = clientRepository.findAll(pageRequest);
        List<Client> clients = clientMapper.toClients(clientBeans);
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Client> updateClient(
            final UUID id,
            final Client client
    ) {
        Optional<ClientBean> optionalClientBean = clientRepository.findById(id);
        if (optionalClientBean.isPresent()) {
            ClientBean clientBean = clientMapper.updateClientBean(client, optionalClientBean.get());
            ClientBean clientBeanUpdated = clientRepository.save(clientBean);
            return new ResponseEntity<>(clientMapper.toClient(clientBeanUpdated), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
}
