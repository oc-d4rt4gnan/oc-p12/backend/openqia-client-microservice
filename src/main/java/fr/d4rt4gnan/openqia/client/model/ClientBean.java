package fr.d4rt4gnan.openqia.client.model;


import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

/**
 * @author D4RT4GNaN
 * @since 03/01/2022
 */
@Entity
@Table(name = "clients", schema = "pgclients")
public class ClientBean {
    
    /**
     * The unique identifier of the ClientBean object in Java and in
     * the Database.
     */
    @Id
    @Column(name = "id")
    private UUID id;
    
    /**
     * The first name of the client.
     * May be empty in case the company name is filled.
     */
    @Column(name = "firstname")
    private String firstName;

    /**
     * The last name of the client.
     * May be empty in case the company name is filled.
     */
    @Column(name = "lastname")
    private String lastName;

    /**
     * The company name of the client.
     * May be empty in case the first and last name are filled.
     */
    @Column(name = "company_name")
    private String companyName;
    
    /**
     * The address associated to the client.
     */
    @Column(name = "address")
    private String address;

    /**
     * The phone number associated to the client (optional).
     */
    @Column(name = "phone_number")
    private String phoneNumber;
    
    /**
     * The email associated to the client (optional).
     */
    @Column(name = "email")
    private String email;

    /**
     * Basic ClientBean constructor
     */
    public ClientBean () {
        firstName   = "";
        lastName    = "";
        companyName = "";
        address     = "";
        phoneNumber = "";
        email       = "";
    }
    
    /**
     * Getter of the "id" attribute.
     *
     * @return the value of the "id" attribute.
     */
    public UUID getId() {
        return id;
    }
    
    /**
     * Setter of the "id" attribute.
     *
     * @param value - The value assigned.
     */
    public void setId(final UUID value) {
        this.id = value;
    }
    
    /**
     * Getter of the "firstName" attribute.
     *
     * @return the value of the "firstName" attribute.
     */
    public String getFirstName() {
        return firstName;
    }
    
    /**
     * Setter of the "firstName" attribute.
     *
     * @param value - The value assigned.
     */
    public void setFirstName(final String value) {
        this.firstName = value;
    }

    /**
     * Getter of the "lastName" attribute.
     *
     * @return the value of the "lastName" attribute.
     */
    public String getLastName() {
        return lastName;
    }
    
    /**
     * Setter of the "lastName" attribute.
     *
     * @param value - The value assigned.
     */
    public void setLastName(final String value) {
        this.lastName = value;
    }

    /**
     * Getter of the "companyName" attribute.
     *
     * @return the value of the "companyName" attribute.
     */
    public String getCompanyName() {
        return companyName;
    }
    
    /**
     * Setter of the "companyName" attribute.
     *
     * @param value - The value assigned.
     */
    public void setCompanyName(final String value) {
        this.companyName = value;
    }
    
    /**
     * Getter of the "address" attribute.
     *
     * @return the value of the "address" attribute.
     */
    public String getAddress() {
        return address;
    }
    
    /**
     * Setter of the "address" attribute.
     *
     * @param value - The value assigned.
     */
    public void setAddress(final String value) {
        this.address = value;
    }

    /**
     * Getter of the "phoneNumber" attribute.
     *
     * @return the value of the "phoneNumber" attribute.
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    /**
     * Setter of the "phoneNumber" attribute.
     *
     * @param value - The value assigned.
     */
    public void setPhoneNumber(final String value) {
        this.phoneNumber = value;
    }
    
    /**
     * Getter of the "email" attribute.
     *
     * @return the value of the "email" attribute.
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * Setter of the "email" attribute.
     *
     * @param value - The value assigned.
     */
    public void setEmail(final String value) {
        this.email = value;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public boolean equals (final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientBean clientBean = (ClientBean) o;
        return Objects.equals(this.id, clientBean.id)
               && Objects.equals(this.firstName, clientBean.firstName)
               && Objects.equals(this.lastName, clientBean.lastName)
               && Objects.equals(this.companyName, clientBean.companyName)
               && Objects.equals(this.address, clientBean.address)
               && Objects.equals(this.phoneNumber, clientBean.phoneNumber)
               && Objects.equals(this.email, clientBean.email);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, companyName, address, phoneNumber, email);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public String toString() {
        return "class Client {\n"
               + "    id: "
               + toIndentedString(id)
               + "\n"
               + "    firstName: "
               + toIndentedString(firstName)
               + "\n"
               + "    lastName: "
               + toIndentedString(lastName)
               + "\n"
               + "    companyName: "
               + toIndentedString(companyName)
               + "\n"
               + "    address: "
               + toIndentedString(address)
               + "\n"
               + "    phoneNumber: "
               + toIndentedString(phoneNumber)
               + "\n"
               + "    email: "
               + toIndentedString(email)
               + "\n"
               + "}";
    }
    
    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     *
     * @param o - The object to be indented.
     * @return the indented string version of the object in parameter.
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
