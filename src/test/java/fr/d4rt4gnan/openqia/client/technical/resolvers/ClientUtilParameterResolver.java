package fr.d4rt4gnan.openqia.client.technical.resolvers;


import fr.d4rt4gnan.openqia.client.technical.utils.ClientUtil;
import fr.d4rt4gnan.openqia.client.technical.utils.impl.ClientUtilImpl;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;


/**
 * @author D4RT4GNaN
 * @since 09/01/2022
 */
public class ClientUtilParameterResolver implements ParameterResolver {
    
    @Override
    public boolean supportsParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == ClientUtil.class;
    }
    
    @Override
    public Object resolveParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return new ClientUtilImpl();
    }
    
}
