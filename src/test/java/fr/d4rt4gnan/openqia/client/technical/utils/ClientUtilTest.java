package fr.d4rt4gnan.openqia.client.technical.utils;


import fr.d4rt4gnan.openqia.client.technical.resolvers.ClientUtilParameterResolver;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author D4RT4GNaN
 * @since 09/01/2022
 */
@ExtendWith(ClientUtilParameterResolver.class)
class ClientUtilTest {
    
    @ParameterizedTest(name = "Given \"{0}\" and \"{1}\", When calling the IsUpdatable method, Then it return {2} !")
    @CsvSource(
            value = {
                    "input, comparator, true",
                    "null, comparator, false",
                    ", comparator, false",
                    "comparator, comparator, false"
            },
            nullValues = "null",
            emptyValue = ""
    )
    void givenInputData_whenCallingIsUpdatableMethod_thenItReturnThatExpected (
            String inputValue,
            String comparatorValue,
            boolean expected,
            ClientUtil clientUtil
    ) {
        // GIVEN -- Parameterized Test
        
        // WHEN
        boolean result = clientUtil.isUpdatable(inputValue, comparatorValue);
    
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
}
