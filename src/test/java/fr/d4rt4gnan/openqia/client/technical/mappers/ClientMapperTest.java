package fr.d4rt4gnan.openqia.client.technical.mappers;


import fr.d4rt4gnan.openqia.client.model.Client;
import fr.d4rt4gnan.openqia.client.model.ClientBean;
import fr.d4rt4gnan.openqia.client.technical.resolvers.ClientMapperParameterResolver;
import fr.d4rt4gnan.openqia.client.technical.utils.ClientUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 03/01/2022
 */
@ExtendWith({ ClientMapperParameterResolver.class, MockitoExtension.class })
class ClientMapperTest {
    
    private UUID   id      = UUID.randomUUID();
    private String firstName = "John";
    private String lastName = "Doe";
    private String companyName = "LexCorp Inc.";
    private String address = "15 rue de l'adresse, ville de France, France";
    private String phoneNumber = "06.12.34.56.78";
    private String email = "nom.prenom@email.com";
    
    @Mock
    private ClientUtil clientUtil;
    
    @Test
    void givenClientInformation_whenCallingToClientBeanMethod_thenGettingInformationInClientBeanFormat (ClientMapper classUnderTest) {
        // GIVEN
        Client client = new Client();
        client.setId(id);
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setCompanyName(companyName);
        client.setAddress(address);
        client.setPhoneNumber(phoneNumber);
        client.setEmail(email);
        
        ClientBean expected = new ClientBean();
        expected.setId(id);
        expected.setFirstName(firstName);
        expected.setLastName(lastName);
        expected.setCompanyName(companyName);
        expected.setAddress(address);
        expected.setPhoneNumber(phoneNumber);
        expected.setEmail(email);
        
        // WHEN
        ClientBean result = classUnderTest.toClientBean(client);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenClientBeanInformation_whenCallingToClientMethod_thenGettingInformationInClientFormat (ClientMapper classUnderTest) {
        // GIVEN
        ClientBean clientBean = new ClientBean();
        clientBean.setId(id);
        clientBean.setFirstName(firstName);
        clientBean.setLastName(lastName);
        clientBean.setCompanyName(companyName);
        clientBean.setAddress(address);
        clientBean.setPhoneNumber(phoneNumber);
        clientBean.setEmail(email);
        
        Client expected = new Client();
        expected.setId(id);
        expected.setFirstName(firstName);
        expected.setLastName(lastName);
        expected.setCompanyName(companyName);
        expected.setAddress(address);
        expected.setPhoneNumber(phoneNumber);
        expected.setEmail(email);
        
        // WHEN
        Client result = classUnderTest.toClient(clientBean);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenClientBeanInformation_whenCallingToClientsMethod_thenGettingInformationInClientFormat (ClientMapper classUnderTest) {
        // GIVEN
        ClientBean clientBean = new ClientBean();
        clientBean.setId(id);
        clientBean.setFirstName(firstName);
        clientBean.setLastName(lastName);
        clientBean.setCompanyName(companyName);
        clientBean.setAddress(address);
        clientBean.setPhoneNumber(phoneNumber);
        clientBean.setEmail(email);
        
        UUID id2 = UUID.randomUUID();
    
        ClientBean clientBean2 = new ClientBean();
        clientBean2.setId(id2);
        clientBean2.setFirstName(firstName);
        clientBean2.setLastName(lastName);
        clientBean2.setCompanyName(companyName);
        clientBean2.setAddress(address);
        clientBean2.setPhoneNumber(phoneNumber);
        clientBean2.setEmail(email);
        
        List<ClientBean> clientBeansList = Arrays.asList(clientBean, clientBean2);
        Page<ClientBean> clientBeans = new PageImpl<>(clientBeansList);
        
        Client client = new Client();
        client.setId(id);
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setCompanyName(companyName);
        client.setAddress(address);
        client.setPhoneNumber(phoneNumber);
        client.setEmail(email);
    
        Client client2 = new Client();
        client2.setId(id2);
        client2.setFirstName(firstName);
        client2.setLastName(lastName);
        client2.setCompanyName(companyName);
        client2.setAddress(address);
        client2.setPhoneNumber(phoneNumber);
        client2.setEmail(email);
    
        List<Client> expected = Arrays.asList(client, client2);
        
        // WHEN
        List<Client> result = classUnderTest.toClients(clientBeans);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenClientUsableToUpdate_whenCallingUpdateClientBeanMethod_thenItReturnClientBeanUpdated (
            ClientMapper clientMapper
    ) {
        // GIVEN
        Client client = new Client();
        client.setFirstName("first name");
        client.setLastName("last name");
        client.setCompanyName("company name");
        client.setAddress("address");
        client.setPhoneNumber("phone number");
        client.setEmail("email@email.com");
        
        ClientBean clientBean = new ClientBean();
        clientBean.setFirstName(firstName);
        clientBean.setLastName(lastName);
        clientBean.setCompanyName("");
        clientBean.setAddress("address");
        clientBean.setPhoneNumber(phoneNumber);
        clientBean.setEmail("");
        
        ClientBean expected = new ClientBean();
        expected.setFirstName("first name");
        expected.setLastName("last name");
        expected.setCompanyName("company name");
        expected.setAddress("address");
        expected.setPhoneNumber("phone number");
        expected.setEmail("email@email.com");
    
        clientMapper.setClientUtil(clientUtil);
        when(clientUtil.isUpdatable(any(), any())).thenReturn(true);
        
        // WHEN
        ClientBean result = clientMapper.updateClientBean(client, clientBean);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenClientUnusableToUpdate_whenCallingUpdateClientBeanMethod_thenItReturnClientBean (
            ClientMapper clientMapper
    ) {
        // GIVEN
        Client client = new Client();
        client.setFirstName("");
        client.setLastName("");
        client.setCompanyName("");
        client.setAddress("");
        client.setPhoneNumber("");
        client.setEmail("");
        
        ClientBean clientBean = new ClientBean();
        clientBean.setFirstName(firstName);
        clientBean.setLastName(lastName);
        clientBean.setCompanyName("");
        clientBean.setAddress("address");
        clientBean.setPhoneNumber(phoneNumber);
        clientBean.setEmail("");
        
        clientMapper.setClientUtil(clientUtil);
        when(clientUtil.isUpdatable(any(), any())).thenReturn(false);
        
        // WHEN
        ClientBean result = clientMapper.updateClientBean(client, clientBean);
        
        // THEN
        assertThat(result).isEqualTo(clientBean);
    }
}
