package fr.d4rt4gnan.openqia.client.business;


import fr.d4rt4gnan.openqia.client.dao.ClientRepository;
import fr.d4rt4gnan.openqia.client.model.Client;
import fr.d4rt4gnan.openqia.client.model.ClientBean;
import fr.d4rt4gnan.openqia.client.technical.mappers.ClientMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 03/01/2022
 */
@ExtendWith(MockitoExtension.class)
class ClientServiceTest {
    
    @InjectMocks
    private ClientService classUnderTest;
    
    @Mock
    private ClientMapper clientMapper;
    @Mock
    private ClientRepository clientRepository;
    
    private UUID id = UUID.randomUUID();
    private String firstName = "John";
    private String lastName = "Doe";
    private String companyName = "LexCorp Inc.";
    private String address = "15 rue de l'adresse, ville de France, France";
    private String phoneNumber = "06.12.34.56.78";
    private String email = "nom.prenom@email.com";
    
    @Test
    void givenClientInformation_whenCallingAddClientMethod_thenItReturnResponse201WithTheSavedClientInformation() {
        // GIVEN
        ClientBean clientBean = new ClientBean();
        clientBean.setId(id);
        clientBean.setFirstName(firstName);
        clientBean.setLastName(lastName);
        clientBean.setCompanyName(companyName);
        clientBean.setAddress(address);
        clientBean.setPhoneNumber(phoneNumber);
        clientBean.setEmail(email);
    
        Client client = new Client();
        client.setId(id);
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setCompanyName(companyName);
        client.setAddress(address);
        client.setPhoneNumber(phoneNumber);
        client.setEmail(email);
        
        when(clientMapper.toClientBean(any())).thenReturn(clientBean);
        when(clientRepository.save(any())).thenReturn(clientBean);
        when(clientMapper.toClient(any())).thenReturn(client);
        
        ResponseEntity<Client> expected = new ResponseEntity<>(client, HttpStatus.CREATED);
        
        // WHEN
        ResponseEntity<Client> result = classUnderTest.addClient(client);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAnExistingID_whenCallingGetClientMethod_thenItReturnTheGoodClient() {
        // GIVEN
        ClientBean clientBean = new ClientBean();
        clientBean.setId(id);
        clientBean.setFirstName(firstName);
        clientBean.setLastName(lastName);
        clientBean.setCompanyName(companyName);
        clientBean.setAddress(address);
        clientBean.setPhoneNumber(phoneNumber);
        clientBean.setEmail(email);
        
        Client client = new Client();
        client.setId(id);
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setCompanyName(companyName);
        client.setAddress(address);
        client.setPhoneNumber(phoneNumber);
        client.setEmail(email);
        
        when(clientRepository.findById(any())).thenReturn(java.util.Optional.of(clientBean));
        when(clientMapper.toClient(any())).thenReturn(client);
        
        ResponseEntity<Client> expected = new ResponseEntity<>(client, HttpStatus.OK);
        
        // WHEN
        ResponseEntity<Client> result = classUnderTest.getClient(id);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAWrongID_whenCallingGetClientMethod_thenItReturnAResponseNotFound() {
        // GIVEN
        when(clientRepository.findById(any())).thenReturn(Optional.empty());
        
        ResponseEntity<Client> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        // WHEN
        ResponseEntity<Client> result = classUnderTest.getClient(id);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenNothing_whenCallingGetClientsMethod_thenItReturnAResponse200() {
        // GIVEN
        List<Client> clients = new ArrayList<>();
        
        when(clientRepository.findAll((Pageable) any())).thenReturn(Page.empty());
        when(clientMapper.toClients(any())).thenReturn(clients);
        
        // WHEN
        ResponseEntity<List<Client>> result = classUnderTest.getClients(0, 5);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
    @Test
    void givenNothing_whenCallingGetClientsMethod_thenItReturnAnEmptyList() {
        // GIVEN
        List<Client> expected = new ArrayList<>();
        
        when(clientRepository.findAll((Pageable) any())).thenReturn(Page.empty());
        when(clientMapper.toClients(any())).thenReturn(expected);
        
        // WHEN
        ResponseEntity<List<Client>> result = classUnderTest.getClients(0, 5);
        
        // THEN
        assertThat(result.getBody()).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenClientWhoExists_whenCallingUpdateClientMethod_thenItReturnAResponseEntityWithCode201() {
        // GIVEN
        UUID id = UUID.randomUUID();
    
        Client client = new Client();
        client.setId(id);
        client.setFirstName("firstname");
        client.setLastName("lastname");
        client.setCompanyName("company name");
        client.setAddress("address");
        client.setPhoneNumber("phone number");
        client.setEmail("email");
        
        ClientBean clientBean = new ClientBean();
    
        when(clientRepository.findById(id)).thenReturn(Optional.of(clientBean));
        when(clientMapper.updateClientBean(any(), any())).thenReturn(clientBean);
        when(clientRepository.save(any())).thenReturn(clientBean);
        when(clientMapper.toClient(any())).thenReturn(client);
        
        // WHEN
        ResponseEntity<Client> result = classUnderTest.updateClient(id, client);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
    
    @Test
    void givenAClientThatDoesNotExist_whenCallingUpdateClientMethod_thenItReturnAResponseEntityWithCode404() {
        // GIVEN
        UUID id = UUID.randomUUID();
    
        Client client = new Client();
        client.setId(id);
        client.setFirstName("firstname");
        client.setLastName("lastname");
        client.setCompanyName("company name");
        client.setAddress("address");
        client.setPhoneNumber("phone number");
        client.setEmail("email");
    
        when(clientRepository.findById(id)).thenReturn(Optional.empty());
        
        // WHEN
        ResponseEntity<Client> result = classUnderTest.updateClient(id, client);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
    
}
